# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at http://mozilla.org/MPL/2.0/.

from .factory import FactoryScraper

from .scraper import (Scraper,
                      DailyScraper,
                      DirectScraper,
                      ReleaseScraper,
                      ReleaseCandidateScraper,
                      TinderboxScraper,
                      TryScraper,
                      )

__all__ = [FactoryScraper,
           Scraper,
           DailyScraper,
           DirectScraper,
           ReleaseScraper,
           ReleaseCandidateScraper,
           TinderboxScraper,
           TryScraper,
           ]
